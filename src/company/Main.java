package company;

public class Main {

    public static void main(String[] args)
    {
        // Create new server and start thread
	    MultiThreadedServer server = new MultiThreadedServer();
	    new Thread(server).start();

	    // Try to accept a new server connection every 2 seconds
	    try
        {
            Thread.sleep(2000);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
