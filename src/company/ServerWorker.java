package company;

import net.tofweb.starlite.*;

import javax.xml.crypto.Data;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;

public class ServerWorker implements Runnable
{
    // Connection with client
    private Socket clientSocket = null;
    private int direct = 0;
    private int[] position = new int[3];
    private Path localPath = new Path();
    // Constructor
    public ServerWorker(Socket clientSocket)
    {
        // Copy socket
        this.clientSocket = clientSocket;
    }


    public ArrayList transferLocal(Path p, int orientation) { //North = 0, East = 1, South = 2, West = 3
        String resp = "";
        String[][] matrix = {
                {"forward","turnRight forward","turnRight,turnRight forward","turnLeft forward"},
                {"turnLeft forward","forward","turnRight forward","turnRight turnRight forward"},
                {"turnRight turnRight forward","turnLeft forward","forward","turnRight forward"},
                {"turnRight forward","turnRight turnRight forward","turnLeft forward","forward"}
        };
        int[] lastPosition = p.get(0).toCoords();
        int neededDirection = 0;
        for (Cell c:p) {

            int[] coordinates = c.toCoords();
            int[] relativeMovement = {coordinates[0] - lastPosition[0],coordinates[1] - lastPosition[1],coordinates[2] - lastPosition[2]};
            if (Arrays.equals(relativeMovement, new int[]{-1, 0, 0})){
                neededDirection = 3;
                resp+=matrix[orientation][neededDirection];
                resp+=" ";
            }
            else if (Arrays.equals(relativeMovement, new int[]{1, 0, 0})){
                neededDirection = 1;
                resp+=matrix[orientation][neededDirection];
                resp+=" ";
            }
            else if (Arrays.equals(relativeMovement, new int[]{0, 0, -1})){
                neededDirection = 2;
                resp+=matrix[orientation][neededDirection];
                resp+=" ";
            }
            else if (Arrays.equals(relativeMovement, new int[]{0, 0, 1})){
                neededDirection = 0;
                resp+=matrix[orientation][neededDirection];
                resp+=" ";
            }
            else if (Arrays.equals(relativeMovement, new int[]{0, -1, 0})){
                resp+="down";
                resp+=" ";
            }
            else if (Arrays.equals(relativeMovement, new int[]{0, 1, 0})){
                resp+="up";
                resp+=" ";
            }
            lastPosition = coordinates;
        }
        ArrayList<Object> returnable = new ArrayList<>();
        returnable.add(neededDirection);
        returnable.add(resp.substring(0, resp.length() - 1));
        return returnable;
    }

    private Path getRoute(int[] c, int[] t){
        CellSpace s = new CellSpace();

        s.setGoalCell(t[0], t[1], t[2]);
        s.setStartCell(c[0], c[1], c[2]);

        CostBlockManager blockManager = new CostBlockManager(s);
        blockManager = Dataset.i().updateBlockMap(s,blockManager);


        Pathfinder pathfinder = new Pathfinder(blockManager);
        return pathfinder.findPath();
        /*String resp = "";
        for (int i = 0; i < path.size(); i++) {
            resp+=","+path.get(i).toString();
        }
        resp = resp.substring(1);
        resp = "{" + resp + "}";
        //return resp;*/
    }

    private String testTask(int[] c, int d){
        String task = "";

        //Route to 1,1,1
        ArrayList x = transferLocal(getRoute(c,new int[]{1,1,1}),d);
        d = (int) x.get(0);
        task+= x.get(1)+" ";

        //Route to 3,3,3
        x = transferLocal(getRoute(new int[]{1,1,1},new int[]{3,3,3}),d);
        d = (int) x.get(0);
        task+=x.get(1)+" ";

        //Route to 1,1,1
        //x = transferLocal(getRoute(c,new int[]{1,1,1}),d);
        //d = (int) x.get(0);
        //task+= x.get(1)+" ";

        //Turn left four times
        direct = d;
        position = new int[]{3,3,3};
        task+="turnLeft turnLeft turnLeft turnLeft";
        return task;
    }

    public String formatLua(String input){
        return "{\""+input.replace(" ","\",\"")+"\"}";
    }

    @Override
    public void run()
    {
        BufferedReader input = null;
        PrintWriter output = null;

        try
        {
            // Get in- and output streams from socket
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new PrintWriter(clientSocket.getOutputStream(), true);

            while (true)
            {
                //[ADDRESS:COMMAND:]
                String inputString = input.readLine();
                System.out.println("Received: " + inputString);
                String[] arguments = inputString.split(":");
                if(arguments.length >= 2){
                    String address = arguments[0];
                    String command = arguments[1];
                    Robot selected = new Robot(address,"Bob", new int[3]);
                    int robotIndex = -1;

                    //Agent selection
                    for (int i = 0; i < Dataset.i().getAgents().size(); i++) {
                        if(Dataset.i().getAgents().get(i).getAddress().equals(address)){
                            robotIndex = i;
                            selected = Dataset.i().getAgents().get(i);
                        }
                    }
                    if (robotIndex==-1){
                        selected = new Robot(address,"Bob", new int[3]);
                        Dataset.i().getAgents().add(selected);
                        robotIndex = Dataset.i().getAgents().indexOf(selected);
                    }

                    //Command response
                    //selected.setPosition(new int[]{Integer.parseInt(arguments[2]),Integer.parseInt(arguments[3]),Integer.parseInt(arguments[4])});
                    if (command.equals("getRobots")){
                        output.println(Dataset.i().getAgents().toString());
                    }

                    if (command.equals("failedMove")){
                        int blockIndex = Integer.parseInt(arguments[2]);
                        //Dataset.i().addObstruction();
                        output.println("SOLVED");
                        System.out.println("Failed movement");
                    }

                    if (command.equals("getTask")) {
                        //getRoute(selected.getPosition(),)
                        String actions = testTask(selected.getPosition(),selected.getDirection());
                        selected.setActions(actions);
                        selected.setDirection(direct);
                        selected.setPosition(position);
                        System.out.println(formatLua(actions));
                        output.println(formatLua(actions));
                    }

                    Dataset.i().getAgents().set(robotIndex,selected);
                }
            }
        }
        // Ignore SocketException/NullPointerException (disconnects)
        catch(SocketException | NullPointerException ignore) {}
        // Catch input/output exceptions
        catch(IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                // Close in- and outputs
                output.close();
                input.close();
                Dataset.i().saveData();
                // Close socket
                clientSocket.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
