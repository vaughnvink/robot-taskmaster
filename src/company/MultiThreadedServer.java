package company;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MultiThreadedServer implements Runnable
{
    private ServerSocket serverSocket = null;
    // Move to config file
    private int port = 4887;
    // Boolean to stop threads
    private boolean isRunning = true;

    public MultiThreadedServer()
    {}

    @Override
    public void run()
    {
        try
        {
            // Create server socket
            serverSocket = new ServerSocket(port);
            System.out.println(serverSocket.getInetAddress());
        }
        catch(IOException e)
        {
            throw new RuntimeException("Cannot open server on port " + port);
        }

        // While server is running, keep accepting connections
        while (isRunning)
        {
            Socket clientSocket;

            try
            {
                // Accept new connection from serverSocket
                clientSocket = serverSocket.accept();
            }
            catch(IOException e)
            {
                throw new RuntimeException("Error connecting to client");
            }

            // Start new ServerWorker thread, with client socket
            new Thread(
                    new ServerWorker(clientSocket)
            ).start();
        }
    }

    private synchronized void stop()
    {
        this.isRunning = false;

        try
        {
            // Close server socket
            this.serverSocket.close();
        }
        catch(IOException e)
        {
            throw new RuntimeException("Error closing server");
        }
    }

    private synchronized boolean isRunning() { return this.isRunning; }
}
