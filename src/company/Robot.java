package company;

import java.util.ArrayList;

/**
 * MineServer - company
 * Created by Vaughn on 31-1-2019.
 */
public class Robot implements java.io.Serializable {
    private String address;
    private String name;
    private int[] position;
    private String actions = "";
    private int direction = 0;

    public Robot(String address, String name, int[] position) {
        this.address = address;
        this.name = name;
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getPosition() {
        return position;
    }

    public void setPosition(int[] position) {
        this.position = position;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
