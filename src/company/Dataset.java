package company;

import net.tofweb.starlite.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * MineServer - company
 * Created by Vaughn on 31-1-2019.
 */
public class Dataset implements java.io.Serializable {
    private static Dataset ourInstance = new Dataset();

    public static Dataset i() {
        return ourInstance;
    }

    private ArrayList<Robot> agents = new ArrayList<>();
    private HashSet<int[]> obstructionMap = new HashSet<>();


    private Dataset() {
        if(!loadData()){
            System.out.println("Error while loading database, will generate fake testing data...");
            generateTestingData();
        }
    }

    public void log(String str){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("log.txt", true));
            writer.append(' ');
            writer.append(str);

            writer.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    public void saveData(){
        try {
            FileOutputStream fileOut = new FileOutputStream("save.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    public boolean loadData(){
        try {
            FileInputStream fis = new FileInputStream("save.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Dataset temp = (Dataset) ois.readObject();
            obstructionMap = temp.getObstructionMap();
            agents = temp.getAgents();
            ois.close();
            fis.close();
            System.out.println("Loaded data!");
            return true;
        } catch(Exception e){
            System.out.println("Failed to load database!");
            //e.printStackTrace();
            return false;
        }
    }
    public void generateTestingData(){

        //Pathfinder pathfinder = new Pathfinder(blockManager);
        //Path path = pathfinder.findPath();
        //System.out.println(path.get(0));
        System.out.println("Testing data added to database!");
    }

    public void addObstruction(int x, int y, int z){
        obstructionMap.add(new int[]{x, y, z});
    }

    public void removeObstruction(int x, int y, int z){
        obstructionMap.remove(new int[]{x, y, z});
    }

    public CostBlockManager updateBlockMap(CellSpace c, CostBlockManager m){
        for (int[] obstruction:obstructionMap) {
            m.blockCell(c.makeNewCell(obstruction[0], obstruction[1], obstruction[2]));
        }
        return m;
    }

    public HashSet<int[]> getObstructionMap() {
        return obstructionMap;
    }

    public void setObstructionMap(HashSet<int[]> obstructionMap) {
        this.obstructionMap = obstructionMap;
    }

    public ArrayList<Robot> getAgents() {
        return agents;
    }

    public void setAgents(ArrayList<Robot> agents) {
        this.agents = agents;
    }
}
